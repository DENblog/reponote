from django.contrib import admin
# modelsからBlogPostクラスをインポート
from .models import ReportPost

# Django管理サイトにBlogPostを登録する
admin.site.register(ReportPost)